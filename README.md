# gin-micro-articles

A simple web-based article management system built using the Gin framework and a microservice architecture.

## Prerequisites

[Go](https://go.dev/doc/install)

## Built With

[Gin](https://github.com/gin-gonic/gin)

## Installation


```bash
# clone the repository
git clone https://gitlab.com/zorulae/gin-micro-articles.git

# navigate into the project directory
cd gin-micro-articles

# build the binary
go build -o app

# start the service
./app
```

The application should now be running on http://localhost:8080.

## Acknowledgments

This project was built following the instructions from this [tutorial](https://semaphoreci.com/community/tutorials/building-go-web-applications-and-microservices-using-gin).

The tutorial covered the basics of building a microservice using Gin.
I've added additional tests for JSON and XML responses.

## Author

[zorulae](https://gitlab.com/zorulae)
